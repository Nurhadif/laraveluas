<?php

use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jabatan')->insert(array(
            array(
                'namajabatan'   => 'Ketua',
                'gaji'      =>'5000000',
                'is_active'     =>1,
                'created_at'    => now(),
            ),
            array(
                'namajabatan'   => 'Wakil Ketua',
                'gaji'      =>'4500000',
                'is_active'     =>1,
                'created_at'    => now(),
            ),
            array(
                'namajabatan'   => 'sekertaris',
                'gaji'      =>'4000000',
                'is_active'     =>1,
                'created_at'    => now(),
            ),
            array(
                'namajabatan'   => 'Bendahara',
                'gaji'      =>'4000000',
                'is_active'     =>1,
                'created_at'    => now(),
            ),
            array(
                'namajabatan'   => 'Karyawan',
                'gaji'      =>'2500000',
                'is_active'     =>1,
                'created_at'    => now(),
            ),
        ));
    }
}
