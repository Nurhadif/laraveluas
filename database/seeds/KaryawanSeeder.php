<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('karyawan')->insert(array(
            array(
                'nama'          => 'Nurhadi',
                'alamat'        => 'Jl darta',
                'lama_bekerja'  => 5,
                'is_active'     =>1,
                'created_at'    => now(),
            ),
            array(
                'nama'          => 'firmansyah',
                'alamat'        => 'Jl aja',
                'lama_bekerja'  => 3,
                'is_active'     =>1,
                'created_at'    => now(),
            ),
        ));
    }
}