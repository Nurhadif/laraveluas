<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('jabatan_id')->unsigned();
            $table->char('nama');
            $table->string('alamat');
            $table->integer('lama_bekerja');
            $table->tinyInteger('is_active')->nullabel()->default(0);
            $table->timestamps();
        });
        Schema::table('karyawan', function (Blueprint $table) {
        
            $table->foreign('jabatan_id')->references('id')->on('jabatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
