<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register','AuthController@getRegister')->name('register')->middleware('guest');
Route::post('/register','AuthController@postRegister')->middleware('guest');
Route::get('/login','AuthController@getlogin')->middleware('guest')->name('login');
Route::post('/login','AuthController@postlogin')->middleware('guest');

Route::get('home', function () {
    return view('home');
})->middleware('auth')->name('home');

Route::get('/logout','AuthController@logout')->middleware('auth')->name('logout');

Route::get('karyawan','KaryawanController@data');
Route::get('karyawan/add','KaryawanController@add');
Route::post('karyawan','KaryawanController@adddata');
Route::get('karyawan/edit/{id}','KaryawanController@edit');
Route::patch('karyawan/{id}','KaryawanController@editdata');
Route::delete('karyawan/{id}','KaryawanController@hapus');
Route::get('karyawan','KaryawanController@data');
Route::get('Karyawan/cari','KaryawanController@cari');
Route::get('serch','KaryawanController@serch')->name('serch_data');


Route::resource('Karyawan', 'KaryawanController');
Route::resource('jabatan', 'JabatanController');