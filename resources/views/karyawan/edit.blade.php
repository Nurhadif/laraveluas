@extends('main')
@section('title', 'Karyawan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Edit Karyawan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><i class="fa fa-dashboard"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="content mt-3">

    <div class="animated fadeIn">

        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <strong>edit Karyawan</strong>
                </div>
                <div class="pull-right">
                    <a href="{{ url('karyawan') }}" class="btn btn-secondary btn-sm">
                        <i class="fa fa-undo"></i>back
                    </a>
                </div>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-4 offset-md-4">
                        <form action="{{ url('karyawan/'.$karyawan->id) }}" method="POST">
                            @method('patch')
                            @csrf
                            <div class="from-group">
                                <label >Jabatan</label>
                                <input type="number" name="jabatan_id" class="form-control" value="{{ $karyawan->jabatan_id }}"  required>
                            </div> 
                            <div class="from-group">
                                <label >Nama Karyawan</label>
                                <input type="text" name="nama" class="form-control" value="{{ $karyawan->nama }}" autofocus required>
                            </div>
                            
                            <div class="from-group">
                                <label >Alamat</label>
                                <input type="text" name="alamat" class="form-control" value="{{ $karyawan->alamat }}">
                            </div> 
                            <div class="from-group">
                                <label >Lama bekerja</label>
                                <input type="number" name="lama_bekerja" class="form-control" value="{{ $karyawan->lama_bekerja }}">
                            </div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        
    </div>

</div>
@endsection