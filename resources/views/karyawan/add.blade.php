@extends('main')
@section('title', 'Karyawan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Tambah Karyawan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><i class="fa fa-dashboard"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="content mt-3">

    <div class="animated fadeIn">

        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <strong>add Karyawan</strong>
                </div>
                <div class="pull-right">
                    <a href="{{ url('karyawan') }}" class="btn btn-secondary btn-sm">
                        <i class="fa fa-undo"></i>back
                    </a>
                </div>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-4 offset-md-4">
                        <form action="{{ url('karyawan') }}" method="POST">
                            @csrf
                            <div class="from-group">
                                <label >Nama Karyawan</label>
                                <input type="text" name="nama" class="form-control @error('nama')
                                 is-invalid @enderror" autofocus >
                                 @error('nama')
                                 <div class="invalid-feedback"> {{ $message }}</div>
                                 @enderror
                            </div>
                            <div class="from-group">
                                <label >jabatan</label>
                                <select class="form-control select2" name="jabatan_id" id="jabatan_id" @error('jabatan_id')
                                is-invalid @enderror>
                                    <option disebled value>pilih jabatan</option>
                                    @foreach ($jabatan as $item)
                                        <option value="{{ $item->id }}">{{ $item->namajabatan }}</option>
                                    @endforeach
                                    @error('jabatan_id')
                                    <div class="invalid-feedback"> {{ $message }}</div>
                                    @enderror  --}}
                                </select>
                            </div>
                                {{-- <input type="number" name="jabatan_id" class="form-control @error('jabatan_id')
                                 is-invalid @enderror" autofocus >
                                 @error('jabatan_id')
                                 <div class="invalid-feedback"> {{ $message }}</div>
                                 @enderror  --}}
                            <div class="from-group">
                                <label >Alamat</label>
                                <input type="text" name="alamat" class="form-control @error('alamat')
                                is-invalid @enderror" >
                                @error('alamat')
                                 <div class="invalid-feedback"> {{ $message }}</div>
                                 @enderror
                            </div> 
                            <div class="from-group">
                                <label >Usia</label>
                                <input type="number" name="lama_bekerja" class="form-control @error('lama_bekerja')
                                is-invalid @enderror" >
                                @error('lama_bekerja')
                                 <div class="invalid-feedback"> {{ $message }}</div>
                                 @enderror
                            </div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        
    </div>

</div>
@endsection