@extends('main')
@section('title', 'Karyawan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Karyawan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><i class="fa fa-dashboard"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="content mt-3">

    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <strong>Data Karyawan</strong>
                </div >
               
                <div class="pull-right">
                    <a href="{{ url('karyawan/add') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>add
                    </a>
                </div>
                {{-- <div class="pull-right"> --}}
                    <form class="navbar-search navbar-search-light form-inline mr-sm-3 float-right" id="navbar-search-main" action="{{route('serch_data')}}">
                        <div class="form-group mb-0">
                          <div class="input-group input-group-alternative input-group-merge">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-search"></i></span>
                            </div>
                            <input class="form-control @error('serch')is-invalid @enderror"  placeholder="Search" type="text" name="keyword">
                            @error('sech')
                                 <div class="invalid-feedback"> {{ $message }}</div>
                                 @enderror
                          </div>
                        </div>
                        {{-- <button type="button" class="fa fa-search" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button> --}}
                      </form>
                    {{-- </div> --}}
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th>id</th>
                        <th>nama</th>
                        <th>Jabatan</th>
                        <th>Alamat</th>
                        <th>Usia</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($karyawan as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->nama }}</td> 
                            <td>{{ $item->jabatan->namajabatan }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->lama_bekerja }}</td>
                            <td class="text-center">
                                {{-- <a href="{{ url('karyawan/'.$item->id) }}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-eye"></i>
                                </a> --}}
                                <a href="{{ url('karyawan/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{ url('karyawan/'.$item->id) }}" method="POST" class="d-inline" onsubmit="return confirm('apakah sudah benar')">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        
    </div>

</div>
@endsection