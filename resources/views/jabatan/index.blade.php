@extends('main')
@section('title', 'Jabatan')
    
@section('breadcrumbs')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Jabatan</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"><i class="fa fa-dashboard"></i></li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="content mt-3">

    <div class="animated fadeIn">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <div class="pull-left">
                    <strong>Jabatan</strong>
                </div>
                {{-- <div class="pull-right">
                    <a href="{{ url('jabatan/add') }}" class="btn btn-success btn-sm">
                        <i class="fa fa-plus"></i>add
                    </a>
                </div> --}}
            </div>
            <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th>id</th>
                        <th>Jabatan</th>
                        <th>Gaji</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($jabatan as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->namajabatan }}</td>
                            <td>{{ $item->gaji }}</td>
                            {{-- <td class="text-center">
                                <a href="{{ url('jabatan/edit/'.$item->id) }}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{ url('jabatan/'.$item->id) }}" method="POST" class="d-inline" onsubmit="return confirm('apakah sudah benar')">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        
    </div>

</div>
@endsection