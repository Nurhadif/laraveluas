<?php

namespace App\Http\Controllers;

use App\Karyawan;
use App\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    public function data()
    {
        // $karyawan = DB::table('karyawan')->get();

        // // return$karyawan;
        // return view('karyawan/data', ['karyawan'=>$karyawan]);

        $karyawan = Karyawan::all();
        
        // return $karyawan;
        return view('karyawan/data', compact('karyawan'));
    }
    public function serch( Request $request ,Karyawan $karyawan){
        $karyawan = karyawan::when($request->keyword, function ($query) use ($request){
            $query->where('nama', 'like', "%{$request->keyword}%");
        })->get();
        // dd($karyawan);

        return view('karyawan.data',compact('karyawan'));
    }
 

    public function add()
    {
        $jabatan = Jabatan::all();
        // $jabatan  = DB::table('jabatan')->where('id', $id)->get();
        return view('karyawan.add', compact('jabatan'));
        // echo$jabatan;
    }

    public function adddata(request $request)
    {
        // return $request;
        $request->validate([
            'jabatan_id' => 'required',
            'nama' => 'required|min:2',
            'alamat' => 'required',
            'lama_bekerja' => 'required',
        ],[
            'nama.required' =>'tidak boleh kosong'
        ]);

        DB::table('karyawan')->insert(
            [
            'jabatan_id' => $request->jabatan_id,
            'nama' => $request->nama,
            'alamat' => $request->alamat, 
            'lama_bekerja' =>$request->lama_bekerja , 
            ]
        );
        return redirect('karyawan')->with('status', 'sukses!');
            
    }

    public function edit($id)
    {
        $karyawan  = DB::table('karyawan')->where('id', $id)->first();
        
        return view('karyawan/edit', compact('karyawan'));
    }

    public function editdata(request $request, $id)
    {
        $request->validate([
            'jabatan_id' => 'required',
            'nama' => 'required|min:2',
            'alamat' => 'required',
            'lama_bekerja' => 'required',
        ]);

        $karyawan = DB::table('karyawan')->where('id', $id)
        ->update( 
            [
            'nama' => $request->nama, 
            'jabatan_id' =>$request->jabatan_id , 
            'alamat' => $request->alamat, 
            'lama_bekerja' =>$request->lama_bekerja ,
            ]); 
        return redirect('karyawan')->with('status', 'sukses!');
    }

    public function hapus($id)
    {
        DB::table('karyawan')->where('id', $id)->delete();
        return redirect('karyawan')->with('status', 'sukses!');
    }


}
